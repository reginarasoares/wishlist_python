from settings import Connection

class Users(Connection):
	def __init__(self):
		Connection.__init__(self)
	
	def search(self, name):
		try:
			sql = f"SELECT * FROM users WHERE name LIKE '{name}'"
			return self.query(sql, name)
		except Exception as e:
			print("Erro ao procurar usuário", e)


class ItemsUsers(Connection):
	def __init__(self):
		Connection.__init__(self)

	def select(self, id):
		try:
			sql = f"SELECT * FROM items_users WHERE id = {id}"
			return self.query(sql)
		except Exception as e:
			print("Erro ao selecionar", e)

	def selectLastItem(self):
		try:
			sql = f"SELECT * FROM items_users ORDER BY id DESC LIMIT 1"
			return self.query(sql)
		except Exception as e:
			print("Erro ao selecionar", e)

	def selectUser(self, name):
		try:
			sql = f"SELECT i.* FROM users u INNER JOIN items_users i ON u.id = i.user_id WHERE u.name = '{name}' ORDER BY i.id"
			return self.query(sql, name)
		except Exception as e:
			print("Erro ao selecionar", e)

	def selectUserNotHave(self, name):
		try:
			sql = f"SELECT i.* FROM users u INNER JOIN items_users i ON u.id = i.user_id WHERE u.name = '{name}' AND i.already_have = false ORDER BY i.id"
			return self.query(sql, name)
		except Exception as e:
			print("Erro ao selecionar", e)
	
	def insert(self, *args):
		try:
			sql = "INSERT INTO items_users (user_id, title, description, link, url_image, already_have) VALUES " + str(args)
			self.execute(sql, args)
			self.commit()
		except Exception as e:
			print("Erro ao inserir", e)

	def updateHave(self, id, *args):
		try:
			sql_u = f"UPDATE items_users SET already_have = %s WHERE id = {id}"
			self.execute(sql_u, args)
			self.commit()
		except Exception as e:
			print("Erro ao atualizar", e)