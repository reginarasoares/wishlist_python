import psycopg2 as db
import psycopg2.extras, json

class Config:
	def __init__(self):
		f = open('../env.json')
		env = json.load(f) 
		self.config = {
			"postgres": {
				"user": env['user'],
				"password": env['password'],
				"host": env['host'],
				"port": env['port'],
				"database": env['database']
			}
		}

class Connection(Config):
	def __init__(self):
		Config.__init__(self)
		try:
			self.conn = db.connect(**self.config["postgres"])
			self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
		except Exception as e:
			print("Erro na conexão", e)
			exit(1)
		
	def __enter__(self):
		return self

	def __exit__(self, exc_type, exc_val, exc_tb):
		self.commit()
		self.connection.close()

	@property
	def connection(self):
		return self.conn

	@property
	def cursor(self):
		return self.cur

	def commit(self):
		self.connection.commit()
	
	def fetchall(self):
		return self.cursor.fetchall()

	def execute(self, sql, params=None):
		self.cursor.execute(sql, params or ())
	
	def query(self, sql, params=None):
		self.cursor.execute(sql, params or ())
		return self.fetchall()