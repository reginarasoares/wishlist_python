import json, falcon, sys, os, random, cgi
from waitress import serve
from falcon_cors import CORS
sys.path.insert(1, r'../data')
import commands


class ListItems:
	def on_get(self, req, res):
		output = {}
		username = req.params['name']

		items = commands.ItemsUsers()

		output = items.selectUser(username.upper())
		
		res.body = json.dumps(output)

	def on_post(self, req, res):
		output = {}
		username = req.params['name']
		data = json.loads(req.stream.read())
		items = commands.ItemsUsers()
		user = commands.Users()

		userdata = user.search(username.upper())
		
		listIds = [row['id'] for row in userdata]
		userId = listIds[0]

		items.insert(userId, data['title'], data['description'], data['link'], data['url_image'], data['already_have'])
		output['value'] = 'Item inserido com sucesso'
		res.body = json.dumps(output)

class ItemImage:
	def on_post(self, req, res):
		items = commands.ItemsUsers()

		listAllItems = items.selectLastItem()
		listIds = [row['id'] for row in listAllItems]
		itemId = listIds[0] + 1
		aux = str(itemId) + '.'

		output = {}
		data = req.stream.read()
		name = req.content_type
		filename = name.replace('/', aux)
		path = os.path.abspath('../../project/src/assets/image')
		imagepath = os.path.join(path, filename)

		with open(imagepath, 'wb') as image_file:
			image_file.write(data)

		output['url_image'] = filename
		res.body = json.dumps(output)


class ItemHave:
	def on_put(self, req, res):
		output = {}
		data = json.loads(req.stream.read()) 
		items = commands.ItemsUsers()

		items.updateHave(data['id'], data['already_have'])

		output['value'] = 'Already_have atualizado com sucesso'

		res.body = json.dumps(output)

class ItemRandom:
	def on_get(self, req, res):
		output = {}
		username = req.params['name']
		items = commands.ItemsUsers()

		itemsList = items.selectUserNotHave(username.upper())

		itemsListIds = [row['id'] for row in itemsList]

		idRandom = random.choice(itemsListIds)

		output = items.select(idRandom)

		res.body = json.dumps(output)


cors = CORS(allow_all_origins=True, allow_all_methods=True, allow_all_headers=True)
api = falcon.API(middleware=[cors.middleware])


api.add_route('/list', ListItems())
api.add_route('/image', ItemImage())
api.add_route('/item-have', ItemHave())
api.add_route('/random', ItemRandom())

serve(api, host="127.0.0.1", port=8001)