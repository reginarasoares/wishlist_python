import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  userName: any
  formBusca:FormGroup

  constructor(private formBuilder: FormBuilder, private router: Router) {
    this.formBusca = this.formBuilder.group({
      name:''
    })
   }


  ngOnInit(): void {
  
  }

  get name(): any {
    return this.formBusca.get('name')
  }

  getUserName() {
    this.userName = this.name.value
    localStorage.setItem('userName', this.userName)
  }

  goToList(){
    window.location.href = "http://localhost:4200/#/list"
  }
}
