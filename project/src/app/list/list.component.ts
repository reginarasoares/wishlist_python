import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  usuario = {
    id: ''
  }

  formItems:FormGroup
  userName: string

  listItems

  randomItem = {
    title: ''
  }

  item = {
    id: '',
    title:'',
    description: '',
    link: '',
    url_image: '',
    already_have: 'false'
  }

  constructor(private apiService: ApiService, private formBuilder: FormBuilder) { 
    this.formItems = this.formBuilder.group({
      id: '',
      title:'',
      description: '',
      link: '',
      url_image: '',
      already_have: 'false'
    })
  }

  get itemData(): any {
    const title = this.formItems.get('title')
    const description = this.formItems.get('description')
    const link = this.formItems.get('link')
    const url_image = this.formItems.get('url_image')
    return [title, description, link, url_image]
  }

  ngOnInit() {
    this.userName = localStorage.getItem('userName');

    this.apiService.getUserId().subscribe((data:any)=>{
      this.listItems = data
    });
  }

  randomItems() {
    this.apiService.getRandomItem().subscribe((data:any)=>{
      this.randomItem = data[0]
    });
  }

  getItem() {
    this.item.title = this.itemData[0].value
    this.item.description = this.itemData[1].value
    this.item.link = this.itemData[2].value
    this.postItem()
    this.formItems.reset()
  }

  postItem(){
    console.log(this.item)
    this.apiService.postList(this.item).subscribe(() => 
      this.apiService.getUserId().subscribe((data:any)=>{
        this.listItems = data
    }))
  }

  putHave(idItem:any, e:any) {
    idItem = parseInt(idItem)
    if(e.target.checked){
      const body = {
        id: idItem,
        already_have: true
      }
      this.apiService.putHave(body).subscribe(() => 
        this.apiService.getUserId().subscribe((data:any)=>{
          this.listItems = data
      }))
    }else{
      const body = {
        id: idItem,
        already_have: false
      }
      this.apiService.putHave(body).subscribe(() => 
        this.apiService.getUserId().subscribe((data:any)=>{
          this.listItems = data
      }))
    }
  }

  uploadImage(file: any){
    const url_image = file[0]
    this.apiService.postImage(url_image).subscribe((data:any) => {
      this.item.url_image = data.url_image
    })
  }
}
