import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  userName : string
  constructor(private httpClient: HttpClient) { 
  }

  public getUserId(){
    this.userName = localStorage.getItem('userName')

    return this.httpClient.get(`http://localhost:8001/list?name=${this.userName}`);
  }

  public getRandomItem(){
    return this.httpClient.get(`http://localhost:8001/random?name=${this.userName}`)
  }

  public postList(body:any){
    return this.httpClient.post(`http://localhost:8001/list?name=${this.userName}`, body)
  }

  public putHave(body:any){
    return this.httpClient.put(`http://localhost:8001/item-have`, body)
  }

  public postImage(body:any){
    return this.httpClient.post(`http://localhost:8001/image`, body)
  }
}
