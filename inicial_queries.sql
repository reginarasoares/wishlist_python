CREATE SEQUENCE users_id_seq INCREMENT 1;
CREATE SEQUENCE items_users_id_seq INCREMENT 1;



CREATE TABLE users(
	id INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('users_id_seq'::regclass),
	name CHARACTER VARYING(80) NOT NULL
);

CREATE TABLE items_users(
	id INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('items_users_id_seq'::regclass),
	user_id INTEGER NOT NULL,
	title CHARACTER VARYING(80) NOT NULL,
	description CHARACTER VARYING(200),
	link CHARACTER VARYING(200),
	url_image CHARACTER VARYING(200),
	already_have BOOL DEFAULT false
);



INSERT INTO users (name) VALUES
	('FERNANDA'), ('JULIANA'), ('KETELYN');

INSERT INTO items_users (user_id, title, description, link, url_image, already_have) VALUES
	(1, 'Xadrez', 'peças de madeira', 'www.mercadolivre.com.br', 'image1.jpeg', false),
	(1, 'Livros', 'Ficção científica, romance, poesia...', 'www.amazon.com.br', 'image2.jpeg', false),
	(2, 'Relogio', 'Dourado com pulseira de couro', 'www.americanas.com.br', 'image3.jpeg', false),
	(2, 'Celular', 'Lançado nos ultimos 2 anos', 'www.magazineluiza.com.br', 'image4.jpeg', false),
	(3, 'Meia Lua', 'Cor preta ou transparente', 'www.pontofrio.com.br', 'image5.jpeg', false),
	(3, 'Ukulele', 'Todos modelos menos baritono', 'www.americanas.com.br', 'image6.jpeg', false);
