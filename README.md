# API - WishList
<br>Gerencia listas de desejos

## Windows
### Procedimentos para rodar API
__1. Instalação do Python e dependências__
<br>Acesse o link abaixo e instale a versão mais recente do python
<br>https://www.python.org/downloads/
<br>Para verificar se a instalação foi bem sucedida, abra o terminal e utilize o comando:
<br>`python --version`
<br>Rode os seguintes comandos para instalar as dependências:
<br> `pip install psycopg2`
<br> `pip install falcon`
<br> `pip install falcon-cors`
<br> `pip install waitress`

__2. Configurações do banco__
<br> Na pasta 'api' crie um arquivo env.json e coloque as configurações do postgreSQL, nesse formato:
```
{
	"user": "*****",
	"password": "*****",
	"host": "127.0.0.1",
	"port": "5432",
	"database": "*******"
}
```
<br> Em seguida execute as queries contidas no arquivo `inicial_queries.sql` que esta na pasta raiz do projeto
<br> No diretório api/middlewares, inicie a API executando o comando
<br> `python routes.py`


### Procedimentos para rodar front-end
__1. Instalação do Node e dependências__
<br>Acesse o link abaixo e instale a versão mais recente do node
<br>https://www.python.org/downloads/
<br>Para verificar se a instalação foi bem sucedida, abra o terminal e utilize o comando:
<br>`node -v`
<br>Na pasta project, rode o comando:
<br> `npm install`

__2. Inicie a API__

__3. Na pasta project, execute o comando__
<br>`ng serve --live-reload false`

__4. Acesse a url http://localhost:4200/#/home__


__Observações__
Foram pré cadastrados 3 usuárias: "Ketelyn", "Juliana" e "Fernanda"
 
